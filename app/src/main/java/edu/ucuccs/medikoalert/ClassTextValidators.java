
package edu.ucuccs.medikoalert;

import android.text.TextUtils;
import android.widget.EditText;

class ClassTextValidators {
    boolean isEmailValid(EditText edt) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(edt.getText().toString()).matches();
    }

    boolean isSameValue(EditText firstValue, EditText secondValue) {
        return TextUtils.equals(firstValue.getText().toString().toLowerCase(), secondValue.getText().toString().toLowerCase());
    }

    boolean isTextEmpty(EditText edt) {
        return edt.getText().toString().trim().length() == 0;
    }

    boolean isAtLeast(EditText edt, int character) {
        return edt.getText().length() >= character;
    }
}
