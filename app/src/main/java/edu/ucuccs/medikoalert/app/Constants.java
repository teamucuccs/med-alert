package edu.ucuccs.medikoalert.app;

public class Constants {
    // Facebook User Fields
    public static final String FB_PROFILE = "public_profile";
    public static final String FB_FRIENDS = "user_friends";
    public static final String FB_EMAIL = "email";
    public static final String FB_BDAY = "user_birthday";
}
