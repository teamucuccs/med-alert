package edu.ucuccs.medikoalert;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

class ClassUserPreferences {

    private SharedPreferences prefs;
    private static final String IS_FIRST_TIME_LAUNCH = "IsThisFirstTimeLaunch";


    ClassUserPreferences(Context cntx) {
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
    }


    void setFirstTimeLaunch(boolean isFirstTime) {
        prefs.edit().putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime).commit();
    }

    boolean isFirstTimeLaunch() {
        return prefs.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }


    int getUserID() {
        return prefs.getInt("user_id", 0);
    }

    void setUserID(int userID) {
        prefs.edit().putInt("user_id", userID).apply();
    }

    String getUserName() {
        return prefs.getString("user_name", "");
    }

    void setUserName(String userName) {
        prefs.edit().putString("user_name", userName).apply();
    }

    void logoutUser() {
        prefs.edit().remove("user_id").apply();
    }

    int getLocationPreference() {
        return prefs.getInt("loc_preference", -1);
    }

    public void setLocationPreference(int location) {
        prefs.edit().putInt("loc_preference", location).apply();
    }

    int getUserType() {
        return prefs.getInt("user_type", 0);
    }

    void setUserType(int userType) {
        prefs.edit().putInt("user_type", userType).apply();
    }

    String getUserPhoto() {
        return prefs.getString("user_image", "");
    }

    void setUserPhoto(String imagePath) {
        prefs.edit().putString("user_image", imagePath).apply();
    }

    String getBannerImage() {
        return prefs.getString("banner_image", "");
    }

    void setBannerImage(String imagePath) {
        prefs.edit().putString("banner_image", imagePath).apply();
    }

    boolean isPushNotificationEnabled() {
        return prefs.getBoolean("prefSwitchNotification", false);
    }

    void pushNotificationEnabled(Boolean value) {
        prefs.edit().putBoolean("prefSwitchNotification", value).apply();
    }

    void setUserAvailability(Boolean value) {
        prefs.edit().putBoolean("prefSwitchAvailability", value).apply();
    }


}
