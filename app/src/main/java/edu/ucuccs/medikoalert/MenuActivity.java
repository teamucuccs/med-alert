package edu.ucuccs.medikoalert;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;

import org.json.JSONException;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import edu.ucuccs.medikoalert.custom.CircleImageTransform;
import edu.ucuccs.medikoalert.model.MyItem;
import edu.ucuccs.medikoalert.utils.GPSTracker;
import edu.ucuccs.medikoalert.utils.MyItemReader;
import io.realm.Realm;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback {

    private ClusterManager<MyItem> mClusterManager;
    private GoogleMap mMap;
    private CameraPosition cameraPosition;
    private GPSTracker mylocation;
    private double mylat, mylng;

    private static final String TAG = MenuActivity.class.getSimpleName();

    private static final int REQUEST_SHOT = 3;

    private static final int REQUEST_STORAGE = 0;
    private static final int REQUEST_IMAGE_CAPTURE = REQUEST_STORAGE + 1;



    private final List<String> tagsListInitial = new ArrayList<>();

    private CoordinatorLayout layoutRoot;
    private TextView mLblResultTags, mLblSelectTag, mLblTip;
    private TextView mLblEmptyState;
    private GoogleApiClient mGoogleApiClient;
    private Uri cameraImageUri = null;
    private TextView lblUsername;

    private ImageView imgResult;
    private ImageView imgEmptyState, imgProfile;

    private Toolbar mToolbar;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;

    private LinearLayout mLinearEmpty;
    NetworkConnectivity mNetConn = new NetworkConnectivity(MenuActivity.this);


    private Realm mRealm;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_menu);

        mylocation = new GPSTracker(getApplicationContext());

        if(mylocation.canGetLocation()) {

            mylocation.getLocation();

        }else{
            mylocation.showSettingsAlert();
        }
        Intent GPSt = new Intent(getApplicationContext(), GPSTracker.class);
        startService(GPSt);


        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("");

        imgResult = (ImageView) findViewById(R.id.img_result);
        mLblResultTags = (TextView) findViewById(R.id.lbl_result_tag);
        mLblEmptyState = (TextView) findViewById(R.id.lbl_empty_state);
        imgEmptyState = (ImageView) findViewById(R.id.img_empty_state);
        mLblTip = (TextView) findViewById(R.id.lbl_tip_of_day);
        mLblSelectTag = (TextView) findViewById(R.id.lbl_select_tag);

        mLinearEmpty = (LinearLayout) findViewById(R.id.layout_empty_state);


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        imgProfile = (ImageView) header.findViewById(R.id.imgProfile);
        lblUsername = (TextView) header.findViewById(R.id.lblUsername);

        lblUsername.setText("Hi " + new ClassUserPreferences(this).getUserName() + "!");
        String strImage = new ClassUserPreferences(this).getUserPhoto();
        Glide.with(this)
                .load(strImage)
                .transform(new CircleImageTransform(this))
                .into(imgProfile);

        setUpToolbar();

        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);

    }

    private void readItems() throws JSONException {
        InputStream inputStream = getResources().openRawResource(R.raw.practitioner);
        List<MyItem> items = new MyItemReader().read(inputStream);
        mClusterManager.addItems(items);
    }

    void setUpToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowHomeEnabled(true);
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    void showNoConnectionState() {
        imgEmptyState.setImageResource(R.drawable.empty_state_onion_connection);
        mLblEmptyState.setText(R.string.msg_no_connection);
        mLblTip.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            startActivity(new Intent(this, ProfileActivity.class));
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        } else if (id == R.id.nav_practitioners) {
        } else if (id == R.id.nav_faqs) {
        }else if (id == R.id.nav_logout) {
            LoginManager.getInstance().logOut(); //Facebook Logout
            signOutFromGplus(); //Google+ Logout
            new ClassUserPreferences(getApplication()).logoutUser();
            finish();
            startActivity(new Intent(MenuActivity.this, ActivitySignUp.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawer.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("deprecation")
    private void signOutFromGplus() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        Log.d(TAG, "onResult: " + status);
                    }
                });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }


        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51.503186, -0.126446), 10));

        mClusterManager = new ClusterManager<MyItem>(this, mMap);
        mMap.setOnCameraIdleListener(mClusterManager);

        try {
            readItems();
        } catch (JSONException e) {
            Toast.makeText(this, "Problem reading list of markers.", Toast.LENGTH_LONG).show();
        }

    }
}
