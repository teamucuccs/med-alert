package edu.ucuccs.medikoalert;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import edu.ucuccs.medikoalert.custom.CircleImageTransform;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProfileActivity extends AppCompatActivity {

    @BindView(R.id.profile_image)
    ImageView imgProfile;
    @BindView(R.id.img_ratings)
    ImageView imgRatings;
    @BindView(R.id.img_since)
    ImageView imgSince;
    @BindView(R.id.lblName)
    TextView lblName;
    @BindView(R.id.lblPlace)
    TextView lblPlace;
    @BindView(R.id.ivEdit)
    ImageView ivEdit;

    //PlaceAutocompleteFragment autocompleteFragment;

    int mUserType;

    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    void hideObjects() {
        imgRatings.setVisibility(View.GONE);
        imgSince.setVisibility(View.GONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        String strImage = new ClassUserPreferences(this).getUserPhoto();
        Glide.with(this)
                .load(strImage)
                .transform(new CircleImageTransform(this))
                .into(imgProfile);
        mUserType = new ClassUserPreferences(getApplicationContext()).getUserType();
        lblName.setText(new ClassUserPreferences(getApplicationContext()).getUserName());
        if (mUserType == 0)
            hideObjects();


    }

    @OnClick(R.id.ivEdit)
    public void editLocation(View view) {

        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                lblPlace.setText(place.getAddress());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Toast.makeText(this, status.toString(), Toast.LENGTH_SHORT).show();

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }


}
