package edu.ucuccs.medikoalert;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import edu.ucuccs.medikoalert.app.Constants;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ActivitySignUp extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = ActivitySignUp.class.getSimpleName();

    @BindView(R.id.btnContinueFacebook)
    Button btnContinueFacebook;
    @BindView(R.id.btnContinueGoogle)
    Button btnContinueGoogle;
    @BindView(R.id.login_button)
    LoginButton fbLogin;
    @BindView(R.id.tvTitle)
    TextView tvTitle;

    private static final int SOCIAL_GOOGLE_RESULT_CODE = 007;
    private static final int SOCIAL_FACEBOOK_RESULT_CODE = 64206;

    private CallbackManager callbackManager;
    private String userFBName, userFBEmail, userFBID, userFBGender, userFBImage;
    private String userGoogleName, userGoogleEmail, userGoogleImage, userGoogleBirthday;
    private boolean hasBirthday = false, hasGender;
    private JSONObject objFacebookData;
    private int LOGIN_ID, userGPlusGender;
    private GoogleApiClient mGoogleApiClient;
    private boolean signedInUser;
    private int mUserType = 0;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        callbackManager = CallbackManager.Factory.create();


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        //Initialize Google
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        ButterKnife.bind(this);
        btnContinueGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectGoogleUser();
            }
        });

        btnContinueFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectFBUser();
            }
        });

    }

    @OnClick(R.id.tvTitle)
    void byPassLogin(View view){
        startActivity(new Intent(ActivitySignUp.this, MenuActivity.class));
    }

    @Override
    public void onStart() {
        Log.d("TAG", "onStart");
        int userIDSession = new ClassUserPreferences(getApplicationContext()).getUserID();
        if (userIDSession != 0) {
            startActivity(new Intent(ActivitySignUp.this, MenuActivity.class));
            finish();
        }

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            getProfileInformation(result);
        } else {
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    getProfileInformation(googleSignInResult);
                }
            });
        }
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult");
        switch (requestCode) {
            case SOCIAL_GOOGLE_RESULT_CODE:
                if (resultCode != RESULT_OK) {
                    signedInUser = false;
                }
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                getProfileInformation(result);
                break;
            case SOCIAL_FACEBOOK_RESULT_CODE:
                callbackManager.onActivityResult(requestCode, resultCode, data);
                break;
        }

    }

    private void getProfileInformation(GoogleSignInResult result) {
        Log.d(TAG, "getProfileInformation:" + (result == null ? "null" : result.getStatus()));
        boolean isSignedIn = (result != null) && result.isSuccess();
        if (isSignedIn) {
            GoogleSignInAccount acct = result.getSignInAccount();
            if (acct != null) {
                if (acct.getDisplayName() != null)
                    userGoogleName = acct.getDisplayName();
                if (acct.getPhotoUrl() != null)
                    userGoogleImage = acct.getPhotoUrl().toString();
                if (acct.getEmail() != null)
                    userGoogleEmail = acct.getEmail();
                userGoogleBirthday = null;
                userGPlusGender = 2;

                if (userGoogleBirthday != null)
                    hasBirthday = true;
                if (userGPlusGender == 0 || userGPlusGender == 1 || userGPlusGender == 2)
                    hasGender = true;
                LOGIN_ID = 0;
                Log.d(TAG, "onCompleted: " + userGoogleName);
                Log.d(TAG, "onCompleted: " + userGoogleEmail);
                Log.d(TAG, "onCompleted: " + userGoogleImage);
                Log.d(TAG, "onCompleted: " + LOGIN_ID);

                registerUser(userGoogleName, userGoogleEmail, userGoogleImage, LOGIN_ID);
            }
        }
    }

    private void registerUser(final String strName, final String strEmail, final String strImage, final int logType) {
        StringRequest postRequest = new StringRequest(Request.Method.POST, Credentials.MEDIKO.BASE_URL + "/register",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", strName);
                params.put("email", strEmail);
                params.put("image_url", strImage);
                params.put("log_type", String.valueOf(logType));
                params.put("user_type", String.valueOf(mUserType));
                new ClassUserPreferences(getApplicationContext()).setUserType(mUserType);
                return params;
            }
        };
        AppController.getInstance().getRequestQueue().add(postRequest);
        new ClassUserPreferences(getApplicationContext()).setUserName(strName);
        new ClassUserPreferences(getApplicationContext()).setUserPhoto(strImage);
        new ClassUserPreferences(getApplication()).setUserID(strEmail.hashCode());
        startActivity(new Intent(this, MenuActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        finish();
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("ActivitySignUp", "onConnectionFailed:" + connectionResult);
    }

    public void selectFBUser(){

        CharSequence options[] = new CharSequence[] {"Patient", "Practitioner"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("I am a");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int user) {
                mUserType = user;
                fbLogin();
            }
        });
        builder.show();
    }

    public void selectGoogleUser(){

        CharSequence options[] = new CharSequence[] {"Patient", "Practitioner"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("I am a");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int user) {
                mUserType = user;
                googlePlusLogin();
            }
        });
        builder.show();
    }

    private void fbLogin(){
        LoginManager.getInstance().logInWithReadPermissions(ActivitySignUp.this, Arrays.asList(Constants.FB_PROFILE, Constants.FB_FRIENDS, Constants.FB_EMAIL, Constants.FB_BDAY));
        fbLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            objFacebookData = response.getJSONObject();
                            Log.v(TAG, objFacebookData.toString());
                            userFBID = objFacebookData.getString("id");
                            userFBName = objFacebookData.getString("name");
                            userFBEmail = objFacebookData.getString("email");
                            userFBGender = objFacebookData.getString("gender");
                            if (userFBGender != null)
                                hasGender = true;
                            userFBImage = "https://graph.facebook.com/" + userFBID + "/picture?type=large";

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        LOGIN_ID = 1;
                        Log.d(TAG, "onCompleted: " + userFBName);
                        Log.d(TAG, "onCompleted: " + userFBEmail);
                        Log.d(TAG, "onCompleted: " + userFBImage);
                        Log.d(TAG, "onCompleted: " + LOGIN_ID);
                        registerUser(userFBName, userFBEmail, userFBImage, LOGIN_ID);
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,gender,birthday,email,first_name,last_name,location,locale,timezone");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException e) {
                if (e instanceof FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();
                    }
                }
            }
        });
    }

    private void googlePlusLogin() {
        Log.d(TAG, "googlePlusLogin");
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, SOCIAL_GOOGLE_RESULT_CODE);
    }

}
